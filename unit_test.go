package paros

import (
	"bytes"
	"errors"
	"testing"
)

func TestParse(t *testing.T) {
	table := []struct {
		in, out string
		err     error
	}{
		{
			in:  "*000114.4567\r\n",
			out: "14.4567",
		},
		{
			in:  "*0001,14.50629, 21.514\r\n",
			out: ",14.50629, 21.514",
		},
		{
			in:  "*0001PI=1000\r\n",
			out: "PI=1000",
		},
	}

	var b bytes.Buffer
	dev := newDq(&b)

	for _, e := range table {
		b.Write([]byte(e.in))
		resp, err := dev.Recv()
		if resp != e.out {
			t.Errorf("Bad response; expected %q, got %q", e.out, resp)
		}
		if !errors.Is(err, e.err) {
			t.Errorf("Expected %v, got %v", e.err, err)
		}
	}
}

func TestSend(t *testing.T) {
	table := []struct {
		addr int
		cmds []string
		out  string
	}{
		{
			addr: 1,
			cmds: []string{"P3"},
			out:  "*0100P3\r\n",
		},
		{
			addr: 1,
			cmds: []string{"EW", "TR=800"},
			out:  "*0100EW*0100TR=800\r\n",
		},
	}

	var b bytes.Buffer
	dev := newDq(&b)

	for _, e := range table {
		b.Reset()
		dev.Send(e.addr, e.cmds...)
		cmd := b.String()
		if cmd != e.out {
			t.Errorf("Bad command format; expected %q, out %q", e.out, cmd)
		}
	}
}

func TestMeasurement(t *testing.T) {
	table := []struct {
		in  string
		out Measurement
	}{
		{
			in:  ",14.50629, 21.514",
			out: Measurement{Pr: 14.50629, Temp: 21.514},
		},
		{
			in:  "14.50629, 21.514",
			out: Measurement{Pr: 14.50629, Temp: 21.514},
		},
		{
			in:  ",14.50629,21.514",
			out: Measurement{Pr: 14.50629, Temp: 21.514},
		},
	}

	for _, e := range table {
		m, err := parseMeasurement(e.in)
		if err != nil {
			t.Error(err)
		}
		if m != e.out {
			t.Errorf("Bad value; expected %#v, got %#v", e.out, m)
		}
	}
}

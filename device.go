package paros

import (
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

type DataType int

const (
	Pressure DataType = iota
	Temperature
)

type Device struct {
	addr int
	dev  *Dq
}

type Info struct {
	// Device serial number
	Sn string `json:"s/n"`
	// Firmware version number
	Vers string `json:"fw_vers"`
	// Device model number
	Model string `json:"model"`
	// Pressure range (psi)
	Prange float64 `json:"prange"`
}

type Response struct {
	Name, Value string
}

type Measurement struct {
	Pr   float64 `json:"pr"`
	Temp float64 `json:"temp"`
}

func parseMeasurement(val string) (Measurement, error) {
	var (
		m   Measurement
		err error
	)

	parts := strings.Split(strings.TrimPrefix(val, ","), ",")
	m.Pr, err = strconv.ParseFloat(strings.Trim(parts[0], " ,\t"), 64)
	if err != nil {
		return m, err
	}
	m.Temp, err = strconv.ParseFloat(strings.Trim(parts[1], " ,\t"), 64)

	return m, err
}

func NewDevice(port io.ReadWriter, addr int) *Device {
	return &Device{addr: addr, dev: newDq(port)}
}

// SendCmd sends one or more commands to a Device and returns the
// response. If the command sets or queries a parameter, then
// Response.Name holds the parameter name and Response.Value holds the
// value, otherwise Response.Value holds the entire response.
func (d *Device) SendCmd(cmds ...string) (Response, error) {
	var resp Response

	err := d.dev.Send(d.addr, cmds...)
	if err != nil {
		return resp, err
	}
	resp.Value, err = d.dev.Recv()
	if idx := strings.Index(resp.Value, "="); idx >= 0 {
		resp.Name = resp.Value[0:idx]
		resp.Value = resp.Value[idx+1:]
	}

	return resp, err
}

// GetInfo returns Device hardware and firmware information.
func (d *Device) GetInfo() (Info, error) {
	var (
		info Info
		resp Response
		err  error
	)

	resp, err = d.SendCmd("SN")
	if err != nil {
		return info, err
	}
	info.Sn = resp.Value

	resp, err = d.SendCmd("VR")
	if err != nil {
		return info, err
	}
	info.Vers = resp.Value

	resp, err = d.SendCmd("MN")
	if err != nil {
		return info, err
	}
	info.Model = strings.TrimRight(resp.Value, " \t")

	resp, err = d.SendCmd("PF")
	if err != nil {
		return info, err
	}
	info.Prange, err = strconv.ParseFloat(resp.Value, 64)

	return info, err
}

// SetIntTime sets the pressure or temperature integration time to a
// resolution of 1 msec.
func (d *Device) SetIntTime(dtype DataType, t time.Duration) error {
	var err error

	val := int64(t / time.Millisecond)
	switch dtype {
	case Pressure:
		_, err = d.SendCmd("EW", fmt.Sprintf("PI=%d", val))
	case Temperature:
		_, err = d.SendCmd("EW", fmt.Sprintf("TI=%d", val))
	default:
		return fmt.Errorf("Invalid data type: %v", dtype)
	}

	return err
}

// IntTime returns the pressure or temperature integration time.
func (d *Device) IntTime(dtype DataType) (time.Duration, error) {
	var (
		val  int
		resp Response
		err  error
	)

	switch dtype {
	case Pressure:
		resp, err = d.SendCmd("PI")
	case Temperature:
		resp, err = d.SendCmd("TI")
	default:
		return 0, fmt.Errorf("Invalid data type: %v", dtype)
	}

	if err != nil {
		return 0, err
	}
	val, err = strconv.Atoi(resp.Value)
	return time.Duration(val) * time.Millisecond, err
}

// Sample returns a single Measurement
func (d *Device) Sample() (Measurement, error) {
	var m Measurement

	resp, err := d.SendCmd("E3")
	if err != nil {
		return m, err
	}
	return parseMeasurement(resp.Value)
}

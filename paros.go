// Package paros provides a communication interface for Paroscientific
// DigiQuartz pressure sensors.
package paros

import (
	"bytes"
	"fmt"
	"io"
	"strings"
	"sync"
	"time"
)

const (
	// Command terminator
	EOL = "\r\n"
	// Data record terminator
	EOR = "\r\n"
	// Start character for commands and responses
	STX = "*"
)

var wakeupTime time.Duration = time.Millisecond * 600

// Dq represents the DigiQuartz communications interface.
type Dq struct {
	port     io.ReadWriter
	respBuf  bytes.Buffer
	rmu, wmu *sync.Mutex
	lastCmd  string
}

// NewDq returns a new Dq attached to port.
func newDq(port io.ReadWriter) *Dq {
	return &Dq{
		port: port,
		rmu:  &sync.Mutex{},
		wmu:  &sync.Mutex{}}
}

func (d *Dq) readUntil(marker []byte) (string, error) {
	d.rmu.Lock()
	defer d.rmu.Unlock()

	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		_, err := d.port.Read(b)
		if err != nil {
			return d.respBuf.String(), err
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

	return "", nil
}

// Sends sends a command to the sensor at address addr.
func (d *Dq) Send(addr int, cmds ...string) error {
	d.wmu.Lock()
	defer d.wmu.Unlock()

	for _, cmd := range cmds {
		d.lastCmd = cmd
		fmt.Fprintf(d.port, "%s%02d00%s",
			STX,
			addr,
			cmd)
	}
	_, err := fmt.Fprint(d.port, EOL)
	return err
}

// Recv returns the next sensor response
func (d *Dq) Recv() (string, error) {
	d.readUntil([]byte(STX))
	resp, err := d.readUntil([]byte(EOL))
	if err != nil {
		return "", err
	}
	return resp[4:], nil
}

// Wakeup wakes all devices from sleep mode.
func (d *Dq) Wakeup() error {
	_, err := d.port.Write([]byte("\n"))
	time.Sleep(wakeupTime)
	return err
}
